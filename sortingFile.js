console.log('sort algorithm');
// console.log(a = [9, 2, 5, 6, 4, 3, 7, 10, 1, 8]);
// set des paramêtres de comptages.

var count = 0;
var swapCount = 0;

// bubble sort
function bubbleSort(a) {

    var swapped;
    // while loop don't stop until nothing left to sort
    do {
        swapped = false;
        // second loop to compare each index with the next one
        for (var i=0; i < a.length-1; i++) {
            if (a[i] > a[i+1]) {
                swap(a, i, i+1);
                swapped = true;
                swapCount ++;
            }
            count ++;
        }

    } while (swapped);

    return a;
};

// selection sort
function selectionSort(a){
    
    for (var i=0; i <= a.length-1; i++){
        //set minimum to this position
        min = i;
        //check the rest of the array to see if anything is smaller
        for (j=i+1; j <= a.length-1; j++){
            if (a[j] < a[min]){
                min = j;
            }
            count ++;
        }
        //if the minimum isn't in the position, swap it
        if (i != min){
            swap(a, i, min);
            swapCount ++;
        }
    }

    return a;
};

function quickSort(a, left, right) {
    
    left = left || 0;
    right = right || a.length - 1;
    
    var swapCount;

    var pivot = right;
    var i = left;

    for(var j = left; j < right; j++) {
        if(a[j] <= a[pivot]) {
          swap(a, i , j);
          swapCount++;
          i = i + 1;
      }
      count ++;
    }

    swap(a, i, j);   
   // console.log('i ' + i);
    

    if(left < i - 1) {
        quickSort(a, left, i - 1);
    }
    if(right > i) {
        quickSort(a, i, right);
    }
    if ((left == i - 1) && (right == i)) {
               

    }

    return a;
}


//swap function
function swap(a, i, j) {
  var temp = a[i];
  a[i] = a[j];
  a[j] = temp;
}
// build an array with random values and a setted number of values
function arrayConstruct(max) {
    var a = [];
    var nb = 0;
    for (var i = 1; i <= max; i++) {
        nb = Math.floor(Math.random() * 1000);
        a.push(nb);
    }
    console.log('array length: ' + max);
    return a;
};


//try the sorting function with croissant arrays 
function sortControl(min, max, sort) {

    var a = [];
    var results = [];
    for (var i = min; i <= max ; i++) {
        a = arrayConstruct(i);
        count = 0;
        var deltaTime = 0;
        var startTime = new Date().getTime();


        sort(a);
        
        deltaTime = new Date().getTime() - startTime;
        console.log('time:' + deltaTime + 'ms');
        console.log('iterations: ' + count);
        console.log('///////////////////////');

        var result = {
            time:deltaTime,
            iteration:count,
            length:a.length
        }    
        results.push(result);
    } 

    return results;
};

//sortControl(3, 100, bubbleSort);
//sortControl(3, 100, selectionSort);
var val = sortControl(3, 100, quickSort);

iterations = [];
arrayLength = [];
for (var i = 0; i <= val.length - 1; i++) {
   iterations.push(val[i].iteration);
   arrayLength.push(val[i].length);
}


console.log(iterations);
console.log(arrayLength);
//console.log(val);


